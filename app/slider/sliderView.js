angular.module('pageSlider.sliderView', ['ngAnimate', 'ui.router'])
    .directive('slider', function () {
        return {
            restrict: 'AE',
            scope: {
                pages: '=',
                currentPage: '='
            },
            link: function (scope) {
                scope.$watch('currentPage', function(page) {
                    page = page || 1;
                    scope.innerCurrentPage = scope.currentPage || page;

                    scope.pages.$promise.then(function() {
                        if (page > scope.pages.length) {
                            page = 1;
                            scope.innerCurrentPage = page;
                        }
                        scope.previousPage = scope.innerCurrentPage - 1 < 1 ? scope.pages.length : scope.innerCurrentPage - 1;
                        scope.nextPage = scope.innerCurrentPage + 1 > scope.pages.length ? 1 : scope.innerCurrentPage + 1;
                        scope.include = scope.pages[page - 1].template;
                    });
                });

                scope.changeState = function(page) {
                    var prevPage = scope.innerCurrentPage;

                    if (page > prevPage) {
                        scope.animationClass = 'slideFromRight';
                    } else {
                        scope.animationClass = 'slideFromLeft';
                    }

                    scope.currentPage = page;
                    scope.innerCurrentPage = page;
                }
            },
            templateUrl: 'slider/sliderView.html'
        };
    });