describe('sliderView', function() {
    var element,
        $compile,
        $rootScope,
        $httpBackend,
        $q;

    beforeEach(module('pageSlider.sliderView'));
    beforeEach(module('slider/sliderView.html'));

    beforeEach(module('pages/page1.html'));
    beforeEach(module('pages/page2.html'));
    beforeEach(module('pages/page3.html'));
    beforeEach(module('pages/page4.html'));
    beforeEach(module('pages/page5.html'));
    beforeEach(module('pages/page6.html'));

    beforeEach(inject(function(_$compile_, _$rootScope_, _$httpBackend_, _$q_){
        $compile = _$compile_;
        $rootScope = _$rootScope_;
        $httpBackend = _$httpBackend_;
        $q = _$q_;
    }));

    describe('changeState', function() {
        beforeEach(function() {
            var deferred = $q.defer();

            $rootScope.pages = [{"name": "Page 1","template": "pages/page1.html"}];
            $rootScope.pages.$promise = deferred.promise;
            deferred.resolve();

            element = angular.element('<slider current-page="1" pages=pages></slider>');
            $compile(element)($rootScope);
            $rootScope.$digest();
        });

        it('should change current page', function() {
            var isoScope = element.isolateScope();
            expect(isoScope.changeState).toBeDefined();

            var oldPage = isoScope.currentPage;
            isoScope.changeState(2);
            expect(isoScope.currentPage).toEqual(2);
        });
    });

    describe('currentPage watcher', function() {
        beforeEach(function() {
            var deferred = $q.defer();

            $rootScope.pages = [{"name": "Page 1","template": "pages/page1.html"},{"name": "Page 2","template": "pages/page2.html"},{"name": "Page 3","template": "pages/page3.html"}];
            $rootScope.pages.$promise = deferred.promise;
            deferred.resolve();

            element = angular.element('<slider current-page="1" pages=pages></slider>');
            $compile(element)($rootScope);
            $rootScope.$digest();
        });

        it('should change included template', function() {
            var isoScope = element.isolateScope();

            isoScope.currentPage = 2;
            isoScope.$digest();
            expect(isoScope.innerCurrentPage).toEqual(isoScope.currentPage);
            expect(isoScope.previousPage).toEqual(1);
            expect(isoScope.nextPage).toEqual(3);
            expect(isoScope.include).toEqual('pages/page2.html');

            isoScope.currentPage = 10;
            isoScope.$digest();
            expect(isoScope.innerCurrentPage).toEqual(1);

            isoScope.currentPage = undefined;
            isoScope.$digest();
            expect(isoScope.innerCurrentPage).toEqual(1);
        });

        it('should use an appropriate animation class depending on page number', function() {
            var isoScope = element.isolateScope();

            isoScope.changeState(2);
            expect(isoScope.animationClass).toEqual('slideFromRight');

            isoScope.changeState(1);
            expect(isoScope.animationClass).toEqual('slideFromLeft');
        });
    });
});