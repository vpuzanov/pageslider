angular.module('pageSlider.sliderServices', ['ngResource'])
    .factory('DataService', ['$resource', function ($resource) {
        return $resource('configs/:sliderViewId.json');
    }]);