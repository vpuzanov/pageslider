angular.module('pageSlider', [
    'ui.router',
    'pageSlider.sliderServices',
    'pageSlider.sliderView'
])
    .controller('appController', ['$scope', '$state', 'DataService', function ($scope, $state, DataService) {
        $scope.pageList0 = DataService.query({sliderViewId: 'sliderView0'});
        $scope.pageList1 = DataService.query({sliderViewId: 'sliderView1'});

        $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
            $scope['currentPage' + toParams.viewId] = toParams.page * 1;
        });

        $scope.$watch('currentPage0', function(page) {
            $state.go('app', {
                viewId: 0,
                page: page || 1
            });
        });

        $scope.$watch('currentPage1', function(page) {
            $state.go('app', {
                viewId: 1,
                page: page || 1
            });
        });
    }])

    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
            .state('app', {
                url: '/viewId/:viewId/page/:page'
            })
    }]);
